﻿using AspNetCore.Cache.Config;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace AspNetCore.Cache
{
    /// <summary>
    /// 
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// 启用redis或内存缓存
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration">Startup 中 的 IConfiguration 对象</param>
        public static void AddRedisOrMemoryCache(this IServiceCollection services, IConfiguration configuration)
        {
            var config = new CacheConfig();
            configuration.GetSection("CacheConfig").Bind(config);
            services.AddSingleton(config); 
            if (config.RedisCachingEnabled)
            {
                services.AddTransient<IRedisConnectionWrapper, RedisConnectionWrapper>();
                services.AddTransient<ICacheManager, RedisCacheManager>();
            }
            else
                services.AddSingleton<ICacheManager, MemoryCacheManager>();
        }

    }
}
