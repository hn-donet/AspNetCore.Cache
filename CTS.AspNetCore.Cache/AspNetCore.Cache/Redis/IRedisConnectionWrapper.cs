﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace AspNetCore.Cache
{
    public interface IRedisConnectionWrapper:IDisposable
    {
        /// <summary>
        /// 获取到Redis内部数据库的交互式连接
        /// </summary>
        /// <param name="db">数据库数量;传递null以使用默认值。</param>
        /// <returns></returns>
        IDatabase GetDatabase(int? db = null);

        /// <summary>
        /// 获取单个服务器的配置API
        /// </summary>
        /// <param name="endPoint"></param>
        /// <returns></returns>
        IServer GetServer(EndPoint endPoint);

        /// <summary>
        /// 获取服务器上定义的所有端点
        /// </summary>
        /// <returns></returns>
        EndPoint[] GetEndPoints();

        /// <summary>
        /// 删除数据库的所有键
        /// </summary>
        /// <param name="db"></param>
        void FlushDatabase(int? db = null);
    }
}
